"use strict"

// //перше завдання
let strArr = ["travel", "hello", "eat", "ski", "lift"];
console.log("Довжина масиву:", strArr.length);

let count = 0;
strArr.forEach((item) => {
    if (item.length > 3) {
        count++;
    }
})

console.log("Кількість елементів в масиві, в яких довжина більше за три:",count);

//завдання два
let users = [
    {
        name: "Іван",
        age: 25,
        sex: "чоловіча"
    },
    {
        name: "Анна",
        age: 19,
        sex: "жіноча"
    },
    {
        name: "Катя",
        age: 29,
        sex: "жіноча"
    },
    {
        name: "Богдан",
        age: 30,
        sex: "чоловіча"
    }
]

console.log("Всі користувачі:", users);

const maleUsers = users.filter(user => user.sex === "чоловіча");

console.log("Користувачі чоловіки:", maleUsers);


///третє завдання
function filterBy(arr, type) {
    return arr.filter(item => typeof item !== type);
}

//перевірка
const filterAudit = filterBy( ['hello', 'world', 23, 14, '23', null, true, {name: "Anna", age:18}], "string");
console.log(filterAudit);







